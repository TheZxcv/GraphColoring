cmake_minimum_required(VERSION 3.0)
project(gcol C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c11 -Wall -Wextra -pedantic\
	-Wmissing-field-initializers\
	-Wswitch-default\
	-Wfloat-equal\
	-Wtype-limits\
	-Wsign-compare\
	-Wignored-qualifiers\
	-Wuninitialized\
	-Wlogical-op\
	-Wshadow\
	-Winit-self")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} ${CMAKE_C_FLAGS} --coverage -fsanitize=address,undefined,leak")
set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS} --coverage")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} ${CMAKE_C_FLAGS} -O2")

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")

include_directories(inc)

IF(CMAKE_BUILD_TYPE MATCHES Debug)
	add_definitions(-DTESTING_BUILD)

	find_package(CMOCKA)
	IF(LIBCMOCKA_FOUND)
		enable_testing()
		add_subdirectory(test)
	ELSE(LIBCMOCKA_FOUND)
		message("Cmocka not found, tests are disabled")
	ENDIF(LIBCMOCKA_FOUND)
ENDIF(CMAKE_BUILD_TYPE MATCHES Debug)

set(INCLUDE_FILES inc/graph.h inc/utils.h inc/logging.h inc/coloring.h inc/tabu_search.h inc/greedy.h inc/shuffle.h
	inc/local_search.h)
set(SOURCE_FILES src/graph.c src/utils.c src/logging.c src/coloring.c src/tabu_search.c src/greedy.c src/shuffle.c
	src/local_search.c)

# Shared static library
add_library(gcol STATIC ${INCLUDE_FILES} ${SOURCE_FILES})

# Main executable
add_executable(gcol-bin src/main.c)
target_link_libraries(gcol-bin gcol)
# Rename output binary to gcol
set_target_properties(gcol-bin PROPERTIES OUTPUT_NAME gcol)

# Coverage generation
add_custom_target(
	cov
	COMMAND lcov -q --capture --initial --directory . -o coverage.base
	COMMAND lcov -q --capture --directory . -o coverage.run
	COMMAND lcov -q --add-tracefile coverage.base --add-tracefile coverage.run -o coverage.unfiltered
	COMMAND lcov -q --remove coverage.unfiltered '${PROJECT_SOURCE_DIR}/test/*.c' -o coverage.total
	COMMAND lcov --summary coverage.total
	WORKING_DIRECTORY "${CMAKE_BUILD_DIR}"
)
