#ifndef TABU_SEACH_H
#define TABU_SEACH_H

#include "coloring.h"
#include "graph.h"

void tabu_search(const graph_t *const graph, coloring_sol_t *solution);

#endif
