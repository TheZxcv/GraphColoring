#ifndef GREEDY_H
#define GREEDY_H

#include "coloring.h"
#include "graph.h"

void greedy(const graph_t *const graph, coloring_sol_t *solution);
void greedy_on_permutations(const graph_t *const graph,
                            coloring_sol_t *solution);
void greedy_fixed_colors(const graph_t *const graph, coloring_sol_t *solution,
                         unsigned int ncolors);

#endif
