#ifndef GRAPH_H
#define GRAPH_H

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

// struct graph_s;
struct graph_s {
  size_t card;
  size_t adj_len;
  bool *adjacencies;
};
typedef struct graph_s graph_t;

graph_t *new_graph(size_t card);
graph_t *graph_from_file(FILE *);
void delete_graph(graph_t *);

inline size_t index_of(size_t n, size_t i, size_t j) {
#ifdef LINEARIZE_GRAPH
  size_t x = MIN(i, j);
  size_t y = MAX(i, j);
  return (n * x) + y - ((x * (x + 1)) / 2);
#else
  return (n * i) + j;
#endif
}

bool dump_graph(const graph_t *const graph,
                const unsigned int *const node_to_color, size_t len);

#endif
