#ifndef LOCAL_SEARCH_H
#define LOCAL_SEARCH_H

#include "coloring.h"

exchange_t find_exchange(const graph_t *const, const coloring_sol_t *,
                         bool *invalid);
void local_search(const graph_t *const graph, coloring_sol_t *sol);
void iterated_local_search(const graph_t *const graph, coloring_sol_t *sol,
                           unsigned int ncolors);

#endif
