#ifndef SUFFLE_H
#define SUFFLE_H

#include <stddef.h>

/* shuffle in place */
void shuffle(void *array, size_t elem_size, size_t len);

#endif
