#ifndef COLORING_H
#define COLORING_H

#include "graph.h"

#include <stddef.h>
#include <stdint.h>

struct coloring_sol_s {
  size_t len;
  unsigned int nconflicts;         // number of conflicts
  unsigned int chromatic;          // number of colors in use
  unsigned int *node_to_color;     // maps node to color
  unsigned int *color_to_node_cnt; // how many nodes with a certain color
};
typedef struct coloring_sol_s coloring_sol_t;

#define INVALID_NODE SIZE_MAX
struct exchange_s {
  size_t node;
  size_t new_color;
  size_t old_color;
};
typedef struct exchange_s exchange_t;
void do_exchange(coloring_sol_t *sol, const graph_t *const, size_t node,
                 size_t new_color);
void do_trivial_exchanges(coloring_sol_t *, const graph_t *const,
                          bool *invalid);

coloring_sol_t *make_solution(const graph_t *const);
coloring_sol_t *new_solution(const graph_t *const);
void reset_solution(coloring_sol_t *);
void clone_solution(coloring_sol_t *dst, const coloring_sol_t *const src);
void randomize_solution(coloring_sol_t *solution, const graph_t *const graph,
                        unsigned int ncolors);
void check_feasibility(const coloring_sol_t *const, const graph_t *const);
void delete_solution(coloring_sol_t *);

#ifdef TESTING_BUILD
unsigned int count_conflicts(const coloring_sol_t *const sol,
                             const graph_t *const graph);
#endif

#endif
