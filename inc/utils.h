#ifndef UTILS_H
#define UTILS_H

#define ALIGN_PADDING(size, alignment)                                         \
  (((alignment) - ((size) % (alignment))) % (alignment))

#ifdef DEBUG
#define assert_always assert
#else
#define assert_always(cond)                                                    \
  do {                                                                         \
    if (!(cond)) {                                                             \
      printf("\n");                                                            \
      log_error("Condition unsatisfied: " #cond);                              \
      exit(EXIT_FAILURE);                                                      \
    }                                                                          \
  } while (false)
#endif

#ifdef TESTING_BUILD
#define TESTING_SCOPE
#else
#define TESTING_SCOPE static
#endif

#define ANSI_GREEN 32
#define ANSI_RED 31

void set_seed(long);
#define randrange(lower, upper)                                                \
  _Generic(((lower) + (upper)), unsigned long long                             \
           : randrange_llu, unsigned long                                      \
           : randrange_lu, unsigned int                                        \
           : randrange_iu, int                                                 \
           : randrange_i)(lower, upper)

unsigned long long randrange_llu(unsigned long long, unsigned long long);
unsigned long randrange_lu(unsigned long, unsigned long);
unsigned int randrange_iu(unsigned int, unsigned int);
int randrange_i(int, int);

int random_color(void);

#endif
