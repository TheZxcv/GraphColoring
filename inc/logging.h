#ifndef LOGGING_H
#define LOGGING_H

enum log_level { LOG_ERROR, LOG_INFO };

#define log_error(...) log_all(LOG_ERROR, __FILE__, __LINE__, __VA_ARGS__)
#define log_info(...) log_all(LOG_INFO, __FILE__, __LINE__, __VA_ARGS__)

#define log_info_inline(...)                                                   \
  log_all_inline(LOG_INFO, __FILE__, __LINE__, __VA_ARGS__)

void log_all(enum log_level level, const char *filename, int lineno,
             const char *fmt, ...);
void log_all_inline(enum log_level level, const char *filename, int lineno,
                    const char *fmt, ...);

void log_set_level(enum log_level level);

#endif
