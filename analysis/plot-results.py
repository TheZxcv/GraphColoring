#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

df = pd.read_csv('results.csv', ',')
df.dropna(inplace=True)
df['error'] = (df['X(G)'] -  df['k found']).abs() / df['X(G)']


df.plot.scatter(x='|E|', y='error')
plt.show()


ax = df.plot.scatter(x='X(G)', y='k found', color='xkcd:coral')
x = np.linspace(*ax.get_xlim())
ax.plot(x, x, color='xkcd:azure')
plt.show()

# Solution Quality Distribution
x = np.sort(df['error'])
y = np.arange(1, len(x)+1) / len(x)
plt.plot(x, y)
plt.show()

# Boxplot
plt.boxplot(df['error'])

# Run Time Distribution
x = np.sort(df['time'])
y = np.arange(1, len(x)+1) / len(x)
plt.plot(x, y)
plt.show()

# Run Time Distribution by graph size
for name, group in df.groupby('|V|'):
  if len(group) > 1:
    x = np.sort(group['time'])
    y = np.arange(1, len(x)+1) / len(x)
    plt.plot(x, y)
    plt.title(name)
    plt.show()
