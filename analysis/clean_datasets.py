#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
from scipy.interpolate import CubicSpline
from sys import argv

if len(argv) < 2:
  print('Usage: {} <dataset.csv>'.format(argv[0]))
  exit()
  
def load_dataset(filename):
  df = pd.read_csv(filename, ',', quotechar='"')
  df.dropna(inplace=True)
  df['error'] = (df['X(G)'] -  df['k found']).abs() / df['X(G)'] * 100
  df['d%'] = 2*df['|E|'] / (df['|V|'] * (df['|V|'] - 1))

  df['X(G)'] = df['X(G)'].fillna(0).astype(int)
  df['k found'] = df['k found'].fillna(0).astype(int)
  df['error'] = df['error'].fillna(0).astype(int)
  return df

datasets = [(name, load_dataset(name)) for name in argv[1:]]

for filename, df in datasets:
  df.sort_values(by='Instance', inplace=True)
  df.to_csv(filename, index=False, quoting=0, float_format='%.3f')
  df.to_latex(filename+'.tex', columns=['Instance', 'X(G)','k found', 'error', 'time'], index=False, float_format='%.3f')

