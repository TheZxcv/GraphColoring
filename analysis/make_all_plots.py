#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
from scipy.interpolate import CubicSpline
from sys import argv
from os.path import basename

def load_dataset(filename):
  df = pd.read_csv(filename, ',')
  df.dropna(inplace=True)
  df['error'] = (df['X(G)'] -  df['k found']).abs() / df['X(G)']
  df['%d'] = 2*df['|E|'] / (df['|V|'] * (df['|V|'] - 1))
  return df

def sqd(dataframe, ax=None, **kargs):
  ax = ax or plt.gca()
  x = np.sort(dataframe['error'])
  y = np.arange(1, len(x)+1) / len(x)
  ax.plot(x, y, **kargs)
  ax.set_ybound(lower=0, upper=1)
  ax.set_xbound(lower=0, upper=2.5)
  ax.grid()

def rtd(dataframe, ax=None, **kargs):
  ax = ax or plt.gca()
  x = np.sort(dataframe['time'])
  y = np.arange(1, len(x)+1) / len(x)
  ax.plot(x, y, **kargs)
  ax.set_ybound(lower=0, upper=1)
  ax.set_xbound(lower=0, upper=0.1)
  ax.grid()

def dataframes_boxplot(dataframes, field, ax=None, **kargs):
  x = 1
  for df in dataframes:
    ax.boxplot(df[field], positions=(x,))
    x += 0.25
  ax.set_xbound(lower=0.5, upper=x+0.25)


names_to_label = {
  "greedy-baseline.csv": "greedy",
  "greedy-perm.csv": "multi-start",
  "ils.csv": "iterated search",
  "localsearch.csv": "local search",
  "tabusearch.csv": "tabu search",
  "results.csv": "any",
}

label_to_color = {
  "greedy": "green",
  "multi-start": "xkcd:azure",
  "iterated search": "xkcd:coral",
  "local search": "purple",
  "tabu search": "red",
  "any": "yellow",
}

if len(argv) < 2:
  print('Usage: {} <dataset.csv>'.format(argv[0]))
  exit()

datasets = [(names_to_label[basename(name)], load_dataset(name)) for name in argv[1:]]

for name, df in datasets:
  sqd(df, label=name, color=label_to_color[name])
plt.gca().set_xlabel('relative solution quality [%]')
plt.gca().set_ylabel('P(solve)')
plt.legend(loc='lower right')
plt.savefig('sqd.pdf', transparent=True)
plt.show()

for name, df in datasets:
  rtd(df, label=name, color=label_to_color[name])
plt.gca().set_xlabel('CPU time [s]')
plt.gca().set_ylabel('P(solve)')
plt.legend(loc='lower right')
plt.savefig('rtd.pdf', transparent=True)
plt.show()

fig, ax = plt.subplots()
ax.set_yscale("log")
ax.set_ybound(lower=1, upper=1000)
ax.set_ylabel('CPU time [s]')
ax.grid()
cdf = pd.concat([df[['time']].assign(Name=name) for name, df in datasets])
boxes = cdf.boxplot(by='Name', ax=ax, patch_artist=True, vert=True, return_type='dict')
for key in boxes[0]:
  for el in boxes[0][key]:
    el.set_color('black')
for (name, _), box in zip(datasets, boxes[0]['boxes']):
  box.set_facecolor(label_to_color[name])
ax.set_title('')
ax.set_xlabel('')
fig.suptitle('')
plt.savefig('time-boxplot.pdf', transparent=True)
plt.show()

fig, ax = plt.subplots()
ax.set_ylabel('relative solution quality [%]')
ax.grid()
cdf = pd.concat([(100*df[['error']]).assign(Name=name).assign(order=i) for i, (name, df) in enumerate(datasets)])
boxes = cdf.boxplot(column='error', by='Name', ax=ax, patch_artist=True, vert=True, return_type='dict')
for key in boxes[0]:
  for el in boxes[0][key]:
    el.set_color('black')
for (name, _), box in zip(datasets, boxes[0]['boxes']):
  box.set_facecolor(label_to_color[name])
ax.set_title('')
ax.set_xlabel('')
fig.suptitle('')
plt.savefig('error-boxplot.pdf', transparent=True)
plt.show()

dim = '|V|'
df = df.sort_values(by=[dim, 'time']).drop_duplicates([dim])
df = df[df['|V|'] < 20000]

ax = df.plot(x=dim, y='time', style='.-', color='xkcd:coral', label='campioni')
ax.set_ylabel('CPU time [s]')
ax.set_xlabel(dim)
xs = np.linspace(*ax.get_xlim())

coefficients = np.polyfit(df[dim], df['time'], deg=3)
#poly = np.poly1d(coefficients)
#plt.plot(xs, poly(xs), color='xkcd:azure', label="poly interp n³")

#ax.plot(x, (x/3816)**3, color='xkcd:azure', label='n³')
#last_one = df.loc[df[dim].idxmax()]
#c = last_one['time'] / last_one[dim]**3
#ax.plot(xs, c*(xs)**3, color='green', label='n³')

c = coefficients[0]
ax.plot(xs, c*(xs)**3, color='xkcd:azure', label='polyfit n³')

plt.grid()
plt.legend(loc='upper left')
ax.set_ybound(lower=-1,upper=20)
plt.savefig('complexity.pdf', transparent=True)
plt.show()
