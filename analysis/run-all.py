#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import subprocess

df = pd.read_csv('dataset.csv', ',')

df['k found'] = [float('nan')]*len(df)
df['time'] = [float('nan')]*len(df)

upper = len(df)
for irow in range(0, upper):
  row = df.iloc[irow]
  print("\r{0:.0%}".format(irow / upper), end='')
  process = subprocess.run(['./gcol', '-q', '-d3', '../instances/{}.col'.format(row.Instance)], encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
  if process.returncode == 0:
    colors, time = process.stdout.strip().split('\n')
    colors = int(colors)
    time = float(time)
    df.loc[irow, 'k found'] = colors
    df.loc[irow, 'time'] = time
print()
print('done!')
df.to_csv('results.csv', index=False)
