#include "coloring.h"
#include "graph.h"
#include "greedy.h"
#include "local_search.h"
#include "logging.h"
#include "shuffle.h"
#include "tabu_search.h"
#include "utils.h"

#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

void log_start(const char *const name);
void log_finish(const char *const name, time_t start, time_t finish);

int graph_coloring(long seed, const char *const filename, bool dump_solution);

typedef void (*coloring_algo_t)(const graph_t *const, coloring_sol_t *);
typedef struct {
  char *name;
  bool enabled;
  coloring_algo_t algorithm;
} algo_entry_t;

void ils_wrapper(const graph_t *const graph, coloring_sol_t *sol) {
  iterated_local_search(graph, sol, sol->chromatic - 1);
}

static algo_entry_t algorithms[] = {
    {"greedy", true, greedy},
    {"greedy perm", true, greedy_on_permutations},
    {"iterated ls", true, ils_wrapper},
    {"local search", true, local_search},
    {"tabu search", true, tabu_search}};
static size_t algorithms_len = sizeof(algorithms) / sizeof(algo_entry_t);

void print_usage(const char *const program) {
  printf("Usage: %s [-qDh] [-s seed] [-d number] [FILE]\n"
         "Options and arguments:\n"
         " -q        quiet, disables all diagnostic messages\n"
         " -D        dumps the solution found to a DOT file (output.dot)\n"
         " -h        prints this usage message\n"
         " -s SEED   uses the argument as seed for the PRNG\n"
         " -d INDEX  disables the algorithm with that INDEX\n"
         "            1. greedy\n"
         "            2. multi-start\n"
         "            3. iterated local search\n"
         "            4. local search\n"
         "            5. tabu search\n",
         program);
}

int main(int argc, char *argv[]) {
  long seed = 0;
  int c;
  unsigned long algo_to_disable;
  bool dump_solution = false;
  opterr = 0;
  errno = 0;
  while ((c = getopt(argc, argv, "qDs:d:h")) != -1) {
    switch (c) {
    case 's':
      seed = strtol(optarg, NULL, 10);
      if (errno != 0) {
        log_error("Error while parsing seed from command line: %s",
                  strerror(errno));
      }
      break;
    case 'd':
      algo_to_disable = strtoul(optarg, NULL, 10);
      if (errno != 0) {
        log_error("Error while parsing seed from command line: %s",
                  strerror(errno));
      } else if (algo_to_disable == 0 || algo_to_disable > algorithms_len) {
        fprintf(stderr, "Invalid algorithm index `%lu`\n", algo_to_disable);
      } else {
        algorithms[algo_to_disable - 1].enabled = false;
      }
      break;
    case 'q':
      log_set_level(LOG_ERROR);
      break;
    case 'D':
      dump_solution = true;
      break;
    case 'h':
      print_usage(argv[0]);
      return EXIT_SUCCESS;
    case '?':
      if (optopt == 's')
        fprintf(stderr, "Option -%c requires a seed.\n", optopt);
      else if (optopt == 'd')
        fprintf(stderr, "Option -%c requires a algorithm number.\n", optopt);
      else
        fprintf(stderr, "Unknown option `-%c'.\n", optopt);
      print_usage(argv[0]);
      return EXIT_FAILURE;
    default:
      assert_always(false);
    }
  }

  return graph_coloring(seed, argv[optind], dump_solution);
}

int graph_coloring(long seed, const char *const filename, bool dump_solution) {
  set_seed(seed);
  log_info("seed: %li", seed);

  time_t starting_time;
  if (filename) {
    FILE *f = fopen(filename, "r");
    if (f == NULL) {
      log_error("Error while opening file");
      return EXIT_FAILURE;
    }
    graph_t *graph = graph_from_file(f);
    fclose(f);
    if (graph == NULL)
      return EXIT_FAILURE;
    int nedges = 0;
    for (size_t i = 0; i < graph->adj_len; i++) {
      if (graph->adjacencies[i])
        nedges++;
    }
    log_info("Cardinality: %zu", graph->card);
    log_info("Edges: %d", nedges);

    time_t start, finish;
    coloring_sol_t *sol = make_solution(graph);
    log_info("Colors: %d", sol->chromatic);

    starting_time = clock();
    for (size_t i = 0; i < algorithms_len; i++) {
      if (algorithms[i].enabled) {
        log_start(algorithms[i].name);
        starting_time = start = clock();
        algorithms[i].algorithm(graph, sol);
        finish = clock();
        log_info("Colors: %u", sol->chromatic);
        log_finish(algorithms[i].name, start, finish);
      }
    }
    finish = clock();

    if (dump_solution)
      dump_graph(graph, sol->node_to_color, sol->len);
    printf("%d\n", sol->chromatic);
    printf("%0.4lf\n", (double)(finish - starting_time) / CLOCKS_PER_SEC);
    delete_solution(sol);
    delete_graph(graph);
  }
  return EXIT_SUCCESS;
}

void log_start(const char *const name) {
  log_info("[\x1b[1m%-12s\x1b[0m] start", name);
}

void log_finish(const char *const name, time_t start, time_t finish) {
  log_info("[\x1b[1m%-12s\x1b[0m] Time: %10.4lf", name,
           (double)(finish - start) / CLOCKS_PER_SEC);
}
