#include "graph.h"
#include "logging.h"
#include "utils.h"

#include <assert.h>
#include <ctype.h>
#include <stdalign.h>
#include <stdbool.h>
#include <stdlib.h>

extern size_t index_of(size_t n, size_t i, size_t j);

graph_t *new_graph(size_t card) {
#ifdef LINEARIZE_GRAPH
  size_t adj_len = (card * (card + 1)) / 2;
#else
  size_t adj_len = card * card;
#endif
  const size_t padding = ALIGN_PADDING(sizeof(graph_t), alignof(bool));
  size_t size = sizeof(graph_t) + padding + adj_len * sizeof(bool);

  void *buf = calloc(1, size);
  if (!buf)
    return NULL;

  graph_t *graph = buf;
  graph->adj_len = adj_len;
  graph->card = card;
  graph->adjacencies = (bool *)((char *)(graph + 1) + padding);
  return graph;
}

void delete_graph(graph_t *graph) { free(graph); }

// format: [edge|edges] {no. nodes} {no. edges}
static bool match_header(FILE *file, unsigned int *nnodes,
                         unsigned int *nedges) {
  int ch;
  while (isspace((ch = fgetc(file))))
    ;
  ungetc(ch, file);
  if (fgetc(file) != 'e')
    return false;
  if (fgetc(file) != 'd')
    return false;
  if (fgetc(file) != 'g')
    return false;
  if (fgetc(file) != 'e')
    return false;
  if ((ch = fgetc(file)) != 's')
    ungetc(ch, file);

  return fscanf(file, " %u %u \n", nnodes, nedges) == 2;
}

graph_t *graph_from_file(FILE *file) {
  int next;
  int lineno = 1;
  graph_t *graph = NULL;
  while ((next = fgetc(file)) != EOF) {
    unsigned int nnodes = 0;
    unsigned int nedges = 0;
    char ch = (char)next;
    switch (ch) {
    case 'c': // comment
      while (fgetc(file) != '\n')
        ;
      break;

    case 'p': // header
      if (graph != NULL) {
        log_error("Error duplicate header");
        goto fail;
      }
      if (!match_header(file, &nnodes, &nedges)) {
        log_error("Error while parsing header");
        goto fail;
      }
      graph = new_graph(nnodes);
      break;

    case 'e': // edge
      if (graph == NULL) {
        log_error("Error missing header");
        goto fail;
      }
      size_t i = 0, j = 0;
      if (fscanf(file, " %lu %lu\n", &i, &j) != 2) {
        log_error("Error while parsing edge");
        goto fail;
      }
      if (i <= 0 || i > graph->card || j <= 0 || j > graph->card) {
        log_error("Indexes out of bounds (%d, %d)", i, j);
        goto fail;
      }
      if (i != j) { // no self loops
        graph->adjacencies[index_of(graph->card, i - 1, j - 1)] = true;
#ifndef LINEARIZE_GRAPH
        graph->adjacencies[index_of(graph->card, j - 1, i - 1)] = true;
#endif
      }
      break;

    default:
      log_error("Unrecognised character while parsing data file");
      goto fail;
    }
    lineno++;
  }
  return graph;

fail:
  log_error("Failed to parse input file, error at line %d", lineno);
  return NULL;
}

bool dump_graph(const graph_t *const graph,
                const unsigned int *const node_to_color, size_t len) {
  bool result = true;
  int *colors_to_rgb = calloc(len, sizeof(int));

  FILE *output = fopen("output.dot", "w");
  if (!output) {
    log_error("Error while opening output file");
    result = false;
    goto cleanup;
  }

  fputs("graph Coloring {\n", output);
  for (size_t node = 0; node < graph->card; node++) {
    if (colors_to_rgb[node_to_color[node]] == 0)
      colors_to_rgb[node_to_color[node]] = random_color();
    fprintf(output, "\t%lu [fillcolor=\"#%06x\",style=filled];\n", node,
            colors_to_rgb[node_to_color[node]]);
  }
  for (size_t node = 0; node < graph->card; node++) {
    for (size_t other = node + 1; other < graph->card; other++) {
      if (graph->adjacencies[index_of(graph->card, node, other)]) {
        fprintf(output, "\t%lu -- %lu;\n", node, other);
      }
    }
  }
  fputs("}\n", output);

cleanup:
  fclose(output);
  free(colors_to_rgb);
  return result;
}
