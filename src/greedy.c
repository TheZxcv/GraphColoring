#include "greedy.h"
#include "shuffle.h"

#include <stdlib.h>

void greedy(const graph_t *const graph, coloring_sol_t *solution) {
  for (size_t node = 0; node < graph->card; node++) {
    for (size_t color = 0; color < solution->node_to_color[node]; color++) {
      bool valid = true;
      for (size_t neigh = 0; neigh < graph->card; neigh++) {
        if (graph->adjacencies[index_of(graph->card, node, neigh)] &&
            solution->node_to_color[neigh] == color) {
          valid = false;
          break;
        }
      }

      if (valid) {
        do_exchange(solution, graph, node, color);
        break;
      }
    }
  }
}

void greedy_fixed_colors(const graph_t *const graph, coloring_sol_t *solution,
                         unsigned int ncolors) {
  for (size_t node = 0; node < graph->card; node++) {
    size_t upper = (ncolors < solution->node_to_color[node])
                       ? ncolors
                       : solution->node_to_color[node];
    for (size_t color = 0; color < upper; color++) {
      bool valid = true;
      for (size_t neigh = 0; neigh < graph->card; neigh++) {
        if (graph->adjacencies[index_of(graph->card, node, neigh)] &&
            solution->node_to_color[neigh] == color) {
          valid = false;
          break;
        }
      }

      if (valid) {
        do_exchange(solution, graph, node, color);
        break;
      }
    }
  }
}

static void _greedy_on_permutation(const graph_t *const graph,
                                   coloring_sol_t *solution,
                                   const size_t *nodes_permutation) {
  for (size_t i = 0; i < graph->card; i++) {
    size_t node = nodes_permutation[i];
    solution->node_to_color[node] = (unsigned int)i;
  }

  for (size_t i = 0; i < graph->card; i++) {
    size_t node = nodes_permutation[i];
    for (size_t color = 0; color < solution->node_to_color[node]; color++) {
      bool valid = true;
      for (size_t neigh = 0; neigh < graph->card; neigh++) {
        if (graph->adjacencies[index_of(graph->card, node, neigh)] &&
            solution->node_to_color[neigh] == color) {
          valid = false;
          break;
        }
      }

      if (valid) {
        do_exchange(solution, graph, node, color);
        break;
      }
    }
  }
}

void greedy_on_permutations(const graph_t *const graph,
                            coloring_sol_t *best_sol) {
  size_t *nodes_permutation = calloc(graph->card, sizeof(size_t));
  for (size_t i = 0; i < graph->card; i++)
    nodes_permutation[i] = i;

  size_t min = best_sol->chromatic;
  coloring_sol_t *tmp = new_solution(graph);
  for (int i = 0; i < 4; i++) {
    reset_solution(tmp);
    shuffle(nodes_permutation, sizeof(size_t), graph->card);
    _greedy_on_permutation(graph, tmp, nodes_permutation);

    if (min > tmp->chromatic) {
      min = tmp->chromatic;
      clone_solution(best_sol, tmp);
    }
  }

  free(nodes_permutation);
  delete_solution(tmp);
}
