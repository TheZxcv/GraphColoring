#include "tabu_search.h"
#include "utils.h"

#include <stdlib.h>
#include <string.h>

#define TABU_SIZE 7
#define MAX_ITERATION 1000

static size_t head = 0;
static size_t tail = 0;
static exchange_t *tabu = NULL;
void add_tabu(const exchange_t *const exchange) {
  tail = (tail + 1) % TABU_SIZE;
  if (head == tail)
    head = (head + 1) % TABU_SIZE;
  tabu[tail] = *exchange;
}

bool is_tabu(size_t node, size_t col) {
  if (head == tail)
    return false;
  size_t curr = head;
  if (tabu[curr].node == node && tabu[curr].old_color == col)
    return true;
  while (curr != tail) {
    curr = (curr + 1) % TABU_SIZE;
    if (tabu[curr].node == node && tabu[curr].old_color == col)
      return true;
  }
  return false;
}

exchange_t find_exchange_with_tabu(const coloring_sol_t *sol,
                                   const graph_t *const graph, bool *invalid) {
  memset(invalid, 0, sol->len * sizeof(bool));
  unsigned int cost = (unsigned int)graph->card;
  exchange_t p = {INVALID_NODE, INVALID_NODE, INVALID_NODE};
  size_t upper_limit = MIN(graph->card, 1000);
  while (upper_limit != 0) {
    upper_limit--;
    size_t v = randrange(0, graph->card - 1);
    invalid[sol->node_to_color[v]] = true;

    for (size_t u = 0; u < graph->card; u++) {
      if (graph->adjacencies[index_of(graph->card, v, u)])
        invalid[sol->node_to_color[u]] = true;
      if (sol->color_to_node_cnt[u] == 0)
        invalid[u] = true;
    }

    for (size_t i = 0; i < sol->len; i++) {
      if (!invalid[i] && cost > sol->color_to_node_cnt[sol->node_to_color[v]]) {
        if (!is_tabu(v, i) ||
            sol->color_to_node_cnt[sol->node_to_color[v]] == 1) {
          p.node = v;
          p.new_color = i;
          p.old_color = sol->node_to_color[v];
          cost = sol->color_to_node_cnt[sol->node_to_color[v]];
          if (cost == 1)
            return p;
        }
      }
    }
    memset(invalid, 0, sol->len * sizeof(bool));
  }
  return p;
}

void tabu_search(const graph_t *const graph, coloring_sol_t *solution) {
  bool *tmp = calloc(solution->len, sizeof(bool));
  tabu = calloc(TABU_SIZE, sizeof(exchange_t));

  for (int i = 0; i < MAX_ITERATION; i++) {
    exchange_t exchange = find_exchange_with_tabu(solution, graph, tmp);

    if (exchange.node != INVALID_NODE) {
      add_tabu(&exchange);
      do_exchange(solution, graph, exchange.node, exchange.new_color);
      do_trivial_exchanges(solution, graph, tmp);
    }
  }
  check_feasibility(solution, graph);
  free(tabu);
  tabu = NULL;
  free(tmp);
}
