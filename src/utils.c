#include "utils.h"

#include <math.h>

static long seed = 0;

#define IA 16807
#define IM 2147483647
#define AM (1.0 / IM)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1 + (IM - 1) / NTAB)
#define EPS 1.2e-7
#define RNMX (1.0 - EPS)

// RAN1 function from Numerical Recipes
double ran1(long *idum) {
  int j;
  long k;
  static long iy = 0;
  static long iv[NTAB];

  if (*idum <= 0 || !iy) {
    if (-(*idum) < 1)
      *idum = 1;
    else
      *idum = -(*idum);
    for (j = NTAB + 7; j >= 0; j--) {
      k = (*idum) / IQ;
      *idum = IA * (*idum - k * IQ) - IR * k;
      if (*idum < 0)
        *idum += IM;
      if (j < NTAB)
        iv[j] = *idum;
    }
    iy = iv[0];
  }
  k = (*idum) / IQ;
  *idum = IA * (*idum - k * IQ) - IR * k;
  if (*idum < 0)
    *idum += IM;
  j = iy / NDIV;
  iy = iv[j];
  iv[j] = *idum;
  if (AM * iy > RNMX)
    return RNMX;
  else
    return AM * iy;
}

void set_seed(long s) { seed = s; }

unsigned long long randrange_llu(unsigned long long lower,
                                 unsigned long long upper) {
  return lower + (unsigned long long)(ran1(&seed) * (upper + 1 - lower - EPS));
}

unsigned long randrange_lu(unsigned long lower, unsigned long upper) {
  return lower + (unsigned long)(ran1(&seed) * (upper + 1 - lower - EPS));
}

unsigned int randrange_iu(unsigned int lower, unsigned int upper) {
  return lower + (unsigned int)(ran1(&seed) * (upper + 1 - lower - EPS));
}

int randrange_i(int lower, int upper) {
  return lower + (int)(ran1(&seed) * (upper + 1 - lower - EPS));
}

static int hsv_to_rgb(double h, double s, double v) {
  int h_i = (int)(h * 6);
  double f = h * 6 - h_i;
  double p = v * (1 - s);
  double q = v * (1 - f * s);
  double t = v * (1 - (1 - f) * s);

  double r, g, b;
  switch (h_i) {
  case 0:
    r = v;
    g = t;
    b = p;
    break;
  case 1:
    r = q;
    g = v;
    b = p;
    break;
  case 2:
    r = p;
    g = v;
    b = t;
    break;
  case 3:
    r = p;
    g = q;
    b = v;
    break;
  case 4:
    r = t;
    g = p;
    b = v;
    break;
  case 5:
  default:
    r = v;
    g = p;
    b = q;
    break;
  }
  int rgb = ((int)(r * 256) << 16) | ((int)(g * 256) << 8) | ((int)(b * 256));
  return rgb;
}

#define ONE_OVER_PHI 0.618033988749895 // Golden ratio conjugate

int random_color(void) {
  double unused = 0;
  double h = ran1(&seed);
  h += ONE_OVER_PHI;
  h = modf(h, &unused);
  return hsv_to_rgb(h, 0.5, 0.95);
}
