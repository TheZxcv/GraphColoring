#include "local_search.h"
#include "coloring.h"
#include "greedy.h"
#include "utils.h"

#include <limits.h>
#include <memory.h>
#include <stdlib.h>

#define MAX_ITERATION 500
#define DEGRADATION_THRESHOLD 5
static size_t TARGET_CHROMATIC = 0;

exchange_t find_exchange(const graph_t *const graph, const coloring_sol_t *sol,
                         bool *invalid) {
  memset(invalid, 0, sol->len * sizeof(bool));
  unsigned int lowest_color_node_cnt = (unsigned int)graph->card;
  exchange_t p = {INVALID_NODE, INVALID_NODE, INVALID_NODE};
  size_t upper_limit = MIN(graph->card, 1000);
  while (upper_limit != 0) {
    upper_limit--;
    size_t v = randrange(0, graph->card - 1);
    invalid[sol->node_to_color[v]] = true;

    for (size_t u = 0; u < graph->card; u++) {
      if (graph->adjacencies[index_of(graph->card, v, u)])
        invalid[sol->node_to_color[u]] = true;
      if (sol->color_to_node_cnt[u] == 0)
        invalid[u] = true;
    }

    for (size_t i = 0; i < sol->len; i++) {
      if (!invalid[i] && lowest_color_node_cnt >
                             sol->color_to_node_cnt[sol->node_to_color[v]]) {
        p.node = v;
        p.new_color = i;
        p.old_color = sol->node_to_color[v];
        lowest_color_node_cnt = sol->color_to_node_cnt[sol->node_to_color[v]];
        if (lowest_color_node_cnt == 1) // sure improvement!
          return p;
      }
    }
    memset(invalid, 0, graph->card * sizeof(bool));
  }
  return p;
}

void local_search(const graph_t *const graph, coloring_sol_t *sol) {
  bool *invalid = calloc(sol->len, sizeof(bool));

  do_trivial_exchanges(sol, graph, invalid);

  bool updated;
  int iter = 0;
  do {
    updated = false;
    exchange_t exchange = find_exchange(graph, sol, invalid);

    if (exchange.node != INVALID_NODE) {
      updated = true;
      do_exchange(sol, graph, exchange.node, exchange.new_color);
      do_trivial_exchanges(sol, graph, invalid);
    }
    iter++;
  } while (updated && iter < MAX_ITERATION);
  check_feasibility(sol, graph);

  free(invalid);
}

exchange_t find_exchange_lowest_conflicts(const graph_t *const graph,
                                          const coloring_sol_t *sol,
                                          unsigned int *conflicts) {
  unsigned int cost = UINT_MAX;
  exchange_t p = {INVALID_NODE, INVALID_NODE, INVALID_NODE};
  for (size_t v = 0; v < graph->card; v++) {
    memset(conflicts, 0, graph->card * sizeof(unsigned int));
    for (size_t u = 0; u < graph->card; u++) {
      if (graph->adjacencies[index_of(graph->card, v, u)])
        conflicts[sol->node_to_color[u]]++;
    }

    size_t curr_color = sol->node_to_color[v];
    for (size_t i = 0; i < sol->len; i++) {
      if (i != curr_color && conflicts[i] < conflicts[curr_color] &&
          conflicts[i] < cost &&
          (sol->color_to_node_cnt[i] != 0 ||
           (sol->chromatic - TARGET_CHROMATIC) < DEGRADATION_THRESHOLD)) {
        p.node = v;
        p.new_color = i;
        p.old_color = curr_color;
        cost = conflicts[i];
        if (cost == 0)
          return p;
      }
    }
  }
  return p;
}

void steepest_descent(coloring_sol_t *sol, const graph_t *const graph,
                      unsigned int *invalid) {
  bool updated;
  int iter = 0;
  do {
    updated = false;
    exchange_t exchange = find_exchange_lowest_conflicts(graph, sol, invalid);

    if (exchange.node != INVALID_NODE) {
      updated = true;
      do_exchange(sol, graph, exchange.node, exchange.new_color);
      do_trivial_exchanges(sol, graph, (bool *)invalid);
    }
    iter++;
  } while (updated && iter < MAX_ITERATION);
}

void perturbate(coloring_sol_t *sol, const graph_t *const graph) {
  int howmany = randrange(1, 20);
  for (int i = 0; i < howmany; i++) {
    size_t node = randrange(0, graph->card - 1);
    size_t color = randrange(0, sol->chromatic - 1);
    if (color != sol->node_to_color[node])
      do_exchange(sol, graph, node, color);
  }
}

void iterated_local_search(const graph_t *const graph, coloring_sol_t *best_sol,
                           unsigned int ncolors) {
  unsigned int *invalid = calloc(best_sol->len, sizeof(unsigned int));
  coloring_sol_t *curr_sol = new_solution(graph);
  clone_solution(curr_sol, best_sol);
  TARGET_CHROMATIC = ncolors;

  randomize_solution(curr_sol, graph, ncolors);
  greedy_fixed_colors(graph, curr_sol, ncolors);
  for (int i = 0; i < 5; i++) {
    perturbate(curr_sol, graph);
    steepest_descent(curr_sol, graph, invalid);

    if (curr_sol->nconflicts == 0 &&
        curr_sol->chromatic < best_sol->chromatic) {
      clone_solution(best_sol, curr_sol);
    }
  }

  free(curr_sol);
  free(invalid);
}
