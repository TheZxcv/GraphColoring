#include "coloring.h"
#include "graph.h"
#include "logging.h"
#include "utils.h"

#include <assert.h>
#include <memory.h>
#include <stdalign.h>
#include <stdint.h>
#include <stdlib.h>

coloring_sol_t *new_solution(const graph_t *const graph) {
  size_t color_len = graph->card;
  size_t colors_size = color_len * sizeof(unsigned int);
  const size_t padding =
      ALIGN_PADDING(sizeof(coloring_sol_t), alignof(unsigned int));
  size_t size = sizeof(coloring_sol_t) + padding + 2 * colors_size;

  void *buf = calloc(1, size);
  if (!buf)
    return NULL;

  coloring_sol_t *sol = buf;
  sol->len = color_len;
  sol->chromatic = 0;
  sol->nconflicts = 0;
  sol->node_to_color = (unsigned int *)((char *)(sol + 1) + padding);
  sol->color_to_node_cnt = &sol->node_to_color[color_len];
  return sol;
}

void reset_solution(coloring_sol_t *sol) {
  for (size_t i = 0; i < sol->len; i++) {
    sol->node_to_color[i] = (unsigned int)i;
    sol->color_to_node_cnt[i] = 1;
  }
  sol->chromatic = (unsigned int)sol->len;
  sol->nconflicts = 0;
}

coloring_sol_t *make_solution(const graph_t *const graph) {
  coloring_sol_t *sol = new_solution(graph);
  if (!sol)
    return NULL;

  reset_solution(sol);
  return sol;
}

void clone_solution(coloring_sol_t *dst, const coloring_sol_t *const src) {
  dst->len = src->len;
  assert_always(dst->len == src->len);
  dst->chromatic = src->chromatic;
  dst->nconflicts = src->nconflicts;
  memcpy(dst->node_to_color, src->node_to_color, src->len * sizeof(int));
  memcpy(dst->color_to_node_cnt, src->color_to_node_cnt,
         src->len * sizeof(int));
}

void delete_solution(coloring_sol_t *sol) { free(sol); }

void check_feasibility(const coloring_sol_t *const sol,
                       const graph_t *const graph) {
  assert_always(sol->nconflicts == 0);
  for (size_t v = 0; v < graph->card; v++) {
    for (size_t u = 0; u < graph->card; u++) {
      if (graph->adjacencies[index_of(graph->card, v, u)]) {
        assert_always(sol->node_to_color[u] != sol->node_to_color[v]);
      }
    }
  }

  unsigned int chromatic = 0;
  for (size_t i = 0; i < sol->len; i++) {
    if (sol->color_to_node_cnt[i] != 0)
      chromatic++;
  }
  assert_always(sol->chromatic == chromatic);
}

static void update_conflicts(coloring_sol_t *sol, const graph_t *const graph,
                             size_t node, size_t old_color) {
  unsigned int old_conflicts = 0;
  unsigned int new_conflicts = 0;
  for (size_t u = 0; u < sol->len; u++) {
    if (u != node && graph->adjacencies[index_of(graph->card, node, u)]) {
      if (sol->node_to_color[u] == old_color)
        old_conflicts++;
      else if (sol->node_to_color[u] == sol->node_to_color[node])
        new_conflicts++;
    }
  }
  if (new_conflicts > old_conflicts)
    sol->nconflicts += new_conflicts - old_conflicts;
  else if (new_conflicts < old_conflicts) {
    sol->nconflicts -= old_conflicts - new_conflicts;
  }
}

void do_exchange(coloring_sol_t *sol, const graph_t *const graph, size_t node,
                 size_t new_color) {
  if (new_color == sol->node_to_color[node])
    return;

  sol->color_to_node_cnt[sol->node_to_color[node]]--;
  sol->color_to_node_cnt[new_color]++;

  size_t old_color = sol->node_to_color[node];
  sol->node_to_color[node] = (unsigned int)new_color;

  if (sol->color_to_node_cnt[old_color] == 0 &&
      sol->color_to_node_cnt[new_color] != 1)
    sol->chromatic--;
  else if (sol->color_to_node_cnt[old_color] != 0 &&
           sol->color_to_node_cnt[new_color] == 1)
    sol->chromatic++;
  update_conflicts(sol, graph, node, old_color);

  log_info_inline("\x1b[%dmColors: %d \x1b[0m-> exchange n:%ld from:%ld "
                  "to:%ld",
                  ((sol->nconflicts == 0) ? ANSI_GREEN : ANSI_RED),
                  sol->chromatic, node, old_color, new_color);
  fflush(stdout);
}

void do_trivial_exchanges(coloring_sol_t *sol, const graph_t *const graph,
                          bool *invalid) {
  for (size_t v = 0; v < graph->card; v++) {
    if (sol->color_to_node_cnt[sol->node_to_color[v]] == 1) {
      // v is the only node with its color
      memset(invalid, 0, sol->len * sizeof(bool));
      invalid[sol->node_to_color[v]] = true;

      // try to find a different non-empty color for v
      for (size_t u = 0; u < graph->card; u++) {
        if (graph->adjacencies[index_of(graph->card, v, u)])
          invalid[sol->node_to_color[u]] = true;
        if (sol->color_to_node_cnt[u] == 0) // non-empty
          invalid[u] = true;
      }

      for (size_t color = 0; color < sol->len; color++) {
        if (!invalid[color]) {
          do_exchange(sol, graph, v, color);
          break;
        }
      }
    }
  }
}

TESTING_SCOPE unsigned int count_conflicts(const coloring_sol_t *const sol,
                                           const graph_t *const graph) {
  unsigned int conflicts = 0;
  for (size_t v = 0; v < graph->card; v++) {
    for (size_t u = v + 1; u < graph->card; u++) {
      if (graph->adjacencies[index_of(graph->card, v, u)] &&
          sol->node_to_color[u] == sol->node_to_color[v]) {
        conflicts++;
      }
    }
  }
  return conflicts;
}

void randomize_solution(coloring_sol_t *solution, const graph_t *const graph,
                        unsigned int ncolors) {
  memset(solution->node_to_color, 0, solution->len * sizeof(unsigned int));
  memset(solution->color_to_node_cnt, 0, solution->len * sizeof(unsigned int));
  for (size_t node = 0; node < graph->card; node++) {
    unsigned int color = randrange(0, ncolors - 1);
    solution->node_to_color[node] = color;
    solution->color_to_node_cnt[color]++;
  }
  solution->chromatic = 0;
  for (size_t color = 0; color < solution->len; color++) {
    if (solution->color_to_node_cnt[color] != 0)
      solution->chromatic++;
  }
  solution->nconflicts = count_conflicts(solution, graph);
}
