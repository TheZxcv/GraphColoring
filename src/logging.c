#include "logging.h"

#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define PATH_SEPARATOR '/'

static bool unterminated_line = false;
static enum log_level logging_level = LOG_INFO;

static char *colors[] = {[LOG_INFO] = "\x1b[1m", [LOG_ERROR] = "\x1b[1;31m"};

static const char *basename(const char *filename) {
  const char *name = strrchr(filename, PATH_SEPARATOR);
  if (name == NULL)
    return filename;
  else
    return name + 1; // skip separator
}

void log_all(enum log_level level, const char *filename, int lineno,
             const char *fmt, ...) {
  if (level > logging_level)
    return;

  FILE *fd = level == LOG_ERROR ? stderr : stdout;
  va_list args;
  if (unterminated_line)
    fputc('\n', fd);
  fputs(colors[level], fd);
  fprintf(fd, "%10s:%3d:\x1b[0m ", basename(filename), lineno);
  va_start(args, fmt);
  vfprintf(fd, fmt, args);
  va_end(args);
  fprintf(fd, "\n");
  unterminated_line = false;
}

void log_all_inline(enum log_level level, const char *filename, int lineno,
                    const char *fmt, ...) {
  if (level > logging_level)
    return;

  FILE *fd = level == LOG_ERROR ? stderr : stdout;
  va_list args;
  fputc('\r', fd);
  fputs(colors[level], fd);
  fprintf(fd, "%s:%d:\x1b[0m ", basename(filename), lineno);
  va_start(args, fmt);
  vfprintf(fd, fmt, args);
  va_end(args);
  fflush(stdout);
  unterminated_line = true;
}

void log_set_level(enum log_level level) { logging_level = level; }
