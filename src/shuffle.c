#include "shuffle.h"
#include "utils.h"

#include <stdlib.h>
#include <string.h>

static void swap(char *x, char *y, char *tmp, size_t elemsize) {
  memcpy(tmp, x, elemsize);
  memcpy(x, y, elemsize);
  memcpy(y, tmp, elemsize);
}

/* Fisher–Yates shuffle algorithm */
void shuffle(void *array, size_t elemsize, size_t len) {
  size_t i, j;
  if (len == 0)
    return;

  void *tmp = malloc(elemsize);
  for (i = len - 1; i > 0; --i) {
    j = randrange(0, i);

    char *elem_i = ((char *)array) + i * elemsize;
    char *elem_j = ((char *)array) + j * elemsize;
    swap(elem_i, elem_j, tmp, elemsize);
  }
  free(tmp);
}
