#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "shuffle.h"

#define DELTA 0.001
#define NELEMS 5
#define UNIFORM_PROB (1.0 / NELEMS)

static void test_uniform_randonmess(void **state) {
  (void)state; /* unused */

  int elements[NELEMS];
  double freq_position_to_elem[NELEMS][NELEMS];

  for (int i = 0; i < NELEMS; i++) {
    elements[i] = i;
    for (int j = 0; j < NELEMS; j++) {
      freq_position_to_elem[i][j] = 0.0;
    }
  }

  int repeats = 500000;
  for (int i = 0; i < repeats; i++) {
    shuffle(elements, sizeof(int), NELEMS);

    for (int j = 0; j < NELEMS; j++) {
      freq_position_to_elem[j][elements[j]] += 1.0;
    }
  }

  for (int i = 0; i < NELEMS; i++) {
    for (int j = 0; j < NELEMS; j++) {
      freq_position_to_elem[i][j] /= repeats;
      assert_true(fabs(UNIFORM_PROB - freq_position_to_elem[i][j]) < DELTA);
    }
  }
}

int main(void) {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_uniform_randonmess),
  };
  return cmocka_run_group_tests(tests, NULL, NULL);
}
