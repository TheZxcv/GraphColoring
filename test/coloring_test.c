#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "coloring.h"

typedef struct {
  graph_t *graph;
  coloring_sol_t *solution;
} testdata_t;

/*
 * edges:
 * 1 - 2
 * 3 - 2
 */
static bool mat[3][3] = {
    {false, false, true},
    {false, false, true},
    {true, true, false},
};

static int setup_graph(void **state) {
  testdata_t *data = malloc(sizeof(testdata_t));
  if (!data)
    goto fail;

  graph_t *graph = new_graph(3);
  if (!graph)
    goto fail;

  for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 3; j++) {
      size_t idx = index_of(graph->card, i, j);
      graph->adjacencies[idx] = mat[i][j];
    }
  }

  coloring_sol_t *sol = make_solution(graph);
  if (!sol)
    goto fail;

  data->graph = graph;
  data->solution = sol;
  *state = data;

  return 0;

fail:
  return -1;
}

static int teardown_graph(void **state) {
  testdata_t *data = (testdata_t *)*state;
  delete_solution(data->solution);
  delete_graph(data->graph);
  free(data);

  return 0;
}

static void test_conflicts(void **state) {
  testdata_t *data = (testdata_t *)*state;
  graph_t *graph = data->graph;
  coloring_sol_t *sol = data->solution;

  sol->chromatic = 1;
  sol->node_to_color[0] = 1;
  sol->node_to_color[1] = 1;
  sol->node_to_color[2] = 1;
  sol->color_to_node_cnt[0] = 0;
  sol->color_to_node_cnt[1] = 3;
  sol->color_to_node_cnt[2] = 0;
  sol->nconflicts = count_conflicts(sol, graph);

  assert_int_equal(2, sol->nconflicts);

  do_exchange(sol, graph, 1, 1);
  assert_int_equal(2, sol->nconflicts);
  assert_int_equal(1, sol->chromatic);

  do_exchange(sol, graph, 1, 0);
  assert_int_equal(1, sol->nconflicts);
  assert_int_equal(2, sol->chromatic);

  do_exchange(sol, graph, 2, 2);
  assert_int_equal(0, sol->nconflicts);
  assert_int_equal(3, sol->chromatic);

  do_exchange(sol, graph, 2, 2);
  assert_int_equal(0, sol->nconflicts);
  assert_int_equal(3, sol->chromatic);

  do_exchange(sol, graph, 2, 0);
  assert_int_equal(1, sol->nconflicts);
  assert_int_equal(2, sol->chromatic);
}

int main(void) {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_conflicts, setup_graph,
                                      teardown_graph),
  };
  return cmocka_run_group_tests(tests, NULL, NULL);
}
