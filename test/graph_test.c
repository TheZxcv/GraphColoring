#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "graph.h"

/*
 * edges:
 * 1 - 2
 * 3 - 2
 */
static bool mat[3][3] = {
    {false, false, true},
    {false, false, true},
    {true, true, false},
};

static int setup_graph(void **state) {
  graph_t *graph = new_graph(3);
  if (!graph)
    return -1;

  for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 3; j++) {
      size_t idx = index_of(graph->card, i, j);
      graph->adjacencies[idx] = mat[i][j];
    }
  }

  *state = graph;

  return 0;
}

static int teardown_graph(void **state) {
  graph_t *graph = (graph_t *)*state;
  delete_graph(graph);

  return 0;
}

static void test_adjacencies(void **state) {
  graph_t *graph = (graph_t *)*state;

  for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 3; j++) {
      size_t idx = index_of(graph->card, i, j);
      assert_true(graph->adjacencies[idx] == mat[i][j]);
    }
  }
}

static void test_read_from_file(void **state) {
  (void)state; /* unused */

  FILE *file = fopen("test-graph.col", "r");
  if (file == NULL) {
    fail_msg("Could not open test file.");
  }

  graph_t *graph = graph_from_file(file);

  assert_non_null(graph);
  assert_int_equal(3, graph->card);
  assert_false(graph->adjacencies[index_of(graph->card, 0, 0)]);

  assert_true(graph->adjacencies[index_of(graph->card, 0, 1)]);
  assert_true(graph->adjacencies[index_of(graph->card, 1, 0)]);
  assert_false(graph->adjacencies[index_of(graph->card, 1, 1)]);

  assert_true(graph->adjacencies[index_of(graph->card, 0, 2)]);
  assert_true(graph->adjacencies[index_of(graph->card, 2, 0)]);

  assert_false(graph->adjacencies[index_of(graph->card, 2, 1)]);
  assert_false(graph->adjacencies[index_of(graph->card, 1, 2)]);
  assert_false(graph->adjacencies[index_of(graph->card, 2, 2)]);

  delete_graph(graph);
}

static void test_read_from_file_alternative_header(void **state) {
  (void)state; /* unused */

  FILE *file = fopen("test-graph-edges.col", "r");
  if (file == NULL) {
    fail_msg("Could not open test file.");
  }

  graph_t *graph = graph_from_file(file);

  assert_non_null(graph);
  assert_int_equal(2, graph->card);
  assert_false(graph->adjacencies[index_of(graph->card, 0, 0)]);
  assert_false(graph->adjacencies[index_of(graph->card, 1, 1)]);

  assert_true(graph->adjacencies[index_of(graph->card, 0, 1)]);
  assert_true(graph->adjacencies[index_of(graph->card, 1, 0)]);

  delete_graph(graph);
}

int main(void) {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_adjacencies, setup_graph,
                                      teardown_graph),
      cmocka_unit_test(test_read_from_file),
      cmocka_unit_test(test_read_from_file_alternative_header),
  };
  return cmocka_run_group_tests(tests, NULL, NULL);
}
