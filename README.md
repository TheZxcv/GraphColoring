# Graph Coloring

## Build

```bash
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
```
The main executable is `gcol`.

## Usage

```bash
$ ./gcol -h
Usage: ./gcol [-qDh] [-s seed] [-d number] [FILE]
Options and arguments:
 -q        quiet, disables all diagnostic messages
 -D        dumps the solution found to a DOT file (output.dot)
 -h        prints this usage message
 -s SEED   uses the argument as seed for the PRNG
 -d INDEX  disables the algorithm with that INDEX
            1. greedy
            2. multi-start
            3. iterated local search
            4. local search
            5. tabu search
```

## Example

```bash
$ ./gcol ../instances/myciel7.col
```
