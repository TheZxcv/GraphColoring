# Problem statement

Il problema della colorazione di un grafo G=(V, E) è definito come segue

C : V -> {1,...,k} è una generica funzione di colorazione che mappa un nodo ad un colore (k \in N, numero di colori)

La funzione di colorazione per essere ammissibile deve rispettare il seguente vincolo
Vv Vw : (v, w) \in E => C(v) \neq C(w)

La funzione ottima C_k*: V -> {1,...,k*} è tale che Vc_k: V -> {1,...,k} k* <= k
Cioè il numero di colori usati da C_k* è il minimo possibile
Questo k* dipende dal grafo ed è chiamata numero cromatico del grafo G, X(G) e il grafo di dice k-cromatico.

 * nominare i grafi cordali come tipologia di grafi facilmente risolvibili con l'algoritmo greedy
