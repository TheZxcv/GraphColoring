\section{Schema risoluzione}

L'algoritmo euristico di risoluzione del problema è composto da una sequenza di
algoritmi mirati a migliorare la soluzione trovata.

Inizialmente viene eseguito l'algoritmo greedy sul grafo di input e su alcune
permutazioni casuali dei nodi del grafo (approccio multi-start).  In questo
modo viene costruita una soluzione di partenza ``buona'' per il problema.

Dato il numero di colori della soluzione trovata prima, viene costruita una
soluzione (anche non ammissibile) con 1 colore in meno partendo da una
soluzione casuale e un algoritmo greedy che non usa più di un numero fissato di
colori.  Tramite \emph{Iterated local search} si tenta di riportare la
soluzione nell'insieme delle ammissibili senza aumentare il numero di colori.

Infine la soluzione migliore viene migliorata tramite una ricerca locale con
steepest descent per scegliere li scambio e una ulteriore
ricerca locale con la meta-euristica tabu search.

\subsection{Algoritmo Greedy}

Come prima passo, viene costruita una soluzione iniziale in cui ogni nodo ha un
colore diverso.  Chiaramente questa soluzione è ammissibile ma lontana
dall'ottimo.

Per migliorarla, viene utilizzato un algoritmo greedy (Alg.~\ref{alg:greedy})
che, dato un ordine sui colori, cambia il colore di ogni nodo ad un altro
colore di ordine più basso se possibile.  Sperimentalmente, si è osservato che
le soluzione trovate da questo algoritmo sono molte spesso ``buone''.

\begin{algorithm}
	\scriptsize
	\caption{Greedy}\label{alg:greedy}
	\begin{algorithmic}[1]
		\Function{Greedy}{$G = \left(V, E\right),\,c : V \rightarrow \left\{1,\ldots k\right\}$}
			\ForAll{$v \in V$}
				%\State{$c[v] \gets \min\left\{j \in \left\{1,\ldots,k\right\} : j \neq c[w] \land v \neq w \;\;\; \forall \left(v,w\right) \in E \right\}$}
				\State{$c[v] \gets \min\left\{j : j \neq c[w] \;\; \forall \left(v,w\right) \in E \right\}$}
			\EndFor{}
			\Return{$c$}
		\EndFunction{}
	\end{algorithmic}
\end{algorithm}

\subsubsection{Permutazione dell'input: multi-start}

L'algoritmo greedy precedente può dare soluzioni diverse a seconda dell'ordine
con cui vengono considerati i nodi del grafo.  Data questa informazione si è
voluto seguire un approccio \emph{multi-start} per ottenere con probabilità
maggiore una soluzione iniziale ``buona''~\cite{Culberson92iteratedgreedy}.

Quindi l'algoritmo costruttivo greedy viene ripetuto su $4$ permutazioni
casuali dei nodi del grafo e la migliore viene utilizzata come soluzione per i
passi successivi.  L'algoritmo è descritto in Alg.~\ref{alg:greedy-perm} dove
$V = \left\{ v_1, \ldots, v_n \right\}$ e $\pi$ è una permutazione di
$\left\{1,\ldots n\right\}$.

\begin{algorithm}[h]
	\scriptsize
	\caption{Greedy con permutazione dei nodi}\label{alg:greedy-perm}
	\begin{algorithmic}[1]
		\Function{Greedy}{$G = \left(V, E\right),\,c : V \rightarrow \left\{1,\ldots k\right\},\,\pi$}
			\ForAll{$i \in \left\{1,\ldots n\right\}$}
				\State{$c[v_{\pi \left( i \right)}] \gets \min\left\{j : j \neq c[w] \;\; \forall \left(v_{\pi\left( i \right)}, w\right) \in E \right\}$}
			\EndFor{}
			\Return{$c$}
		\EndFunction{}
	\end{algorithmic}
\end{algorithm}

\subsection{Definizione scambio}

Uno scambio è definito come il cambio di colore di un singolo nodo del grafo da
un colore $i$ ad un colore $j$ tali che $i\neq j$.

La funzione che esegue questo scambio aggiorna anche la struttura dati che
tiene traccia dei conflitti nel caso in cui la soluzione su cui stiamo
lavorando è nell'insieme delle non ammissibili.

\subsection{Iterated local search}

Supponiamo che la soluzione ammissibile trovata dai passi precedenti sia
composta da $k$ colori.  In questo passo viene costruita una soluzione casuale
(anche non ammissibile, $ \notin X$) utilizzando $k-1$ colori tramite la
procedura \FunctionName{randomize\_solution}.  Dopodiché sulla soluzione
casuale viene applicata una versione dell'algoritmo Greedy chiamata
\FunctionName{greedy\_fixed\_colors}.  Questa si comporta esattamente con
l'algoritmo Greedy normale con l'eccezione di non introdurre nuovi colori altre
i $k-1$.

Ora per $5$ volte la soluzione viene perturbata e migliorata tramite ricerca
locale.  Se una delle soluzioni trovate è ammissibile ($\in X$) e migliorante,
allora viene sostituita alla migliore trovata fino a questo punto.

\begin{algorithm}[h]
	\scriptsize
	\caption{Iterated Local Search}\label{alg:ils}
	\begin{algorithmic}[1]
		\Function{IteratedLocalSearch}{$S'$}
			\State{$S \gets$ randomize\_solution($k-1$)}
			\State{greedy\_fixed\_colors($S, k-1$)}
			\For{$5$ times}
				\State{perturbate($S$)}
				\State{steepest\_descent($S$)}
				\If{$S \in X \land S < S'$}
					\State{$S' \gets S$}
				\EndIf{}
			\EndFor{}
			\Return{$S'$}
		\EndFunction{}
	\end{algorithmic}
\end{algorithm}

La perturbazione consiste in un numero casuale (al massimo $20$) di scambi
casuali.  La funzione di ricerca della scambio utilizzata dalla \emph{Steepest
Descent} scegli uno scambio che diminuisce il numero di conflitti e che non
aumenta il numero di colori usati oltre una certa soglia fissata a $5$.
\medskip

L'approccio descritto si è rilevato piuttosto fallimentare.  In molti casi
questa procedura richiede parecchio tempo e conclude la ricerca senza trovare
soluzioni ammissibili.

\subsection{Local search}

La ricerca locale viene eseguita per $500$ iterazioni se ad ogni iterazione
viene effettuato fatto uno scambio.  Gli scambi sono cercati esaminando una
parte scelta casualmente dell'intorno e considerando solo i colori già in uso.
Questa euristica per la scelta dello scambio cerca di concentrare tutti i nodi
in meno colori possibile.

\subsection{Tabu search}

Infine viene effettuata una ulteriore ricerca locale questa volta utilizzando
la meta-euristica \emph{tabu search} che vieta la ripetizioni di scambi già
effettuati per un determinato lasso di tempo.  La grandezza della collezione
dei divieti è stata fissata a $7$~\cite{Hertz1987}.

La ricerca degli scambi nell'intorno viene effettuata allo stesso modo della
\emph{local search} con l'unica differenza della presenza di divieti.

\subsection{Complessità in tempo}

Siano $n = \lvert V \rvert$ e $m = \lvert E \rvert$.  La complessità in tempo
dell'algoritmo greedy è pari a $O(n^3)$ dato che per ogni nodo vanno
considerati tutti i colori rispetto tutte le coppie $(v, w) \in E$:

\[
	T(\mbox{\scriptsize \FunctionName{greedy}}) = O\left( n^2(n-1)\right) = O(n^3)
\]

Nel caso in cui si permetta alla soluzione di uscire dall'insieme delle
soluzioni ammissibili, la funzione di scambio, che di solito è $O(1)$, diventa
di complessità lineare dato che deve aggiornare una struttura dati che tiene
conto dei conflitti presenti:

\[
	T(\mbox{\scriptsize \FunctionName{exchange}}) = O(n)
\]


Le varie funzioni di ricerca dell'intorno hanno una complessità in tempo simile
pari a:

\[
	T(\mbox{\scriptsize \FunctionName{find\_exchange}}) = O(n^2)
\]

La complessità totale ottenuta considerando tutti gli step è $O(n^{3})$.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{figures/complexity}
	\caption{Tempo di esecuzione in relazione al numero di nodi.}\label{fig:complexity}
\end{figure}
